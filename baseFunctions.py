#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 15:18:03 2018

@author: aman
"""
import numpy as np
import os
import glob
import re
import sys
if  sys.version_info[0]<3:
    import Tkinter as tk
    import tkFileDialog as tkd
    from itertools import izip as iterIzip
else:
    import tkinter as tk
    from tkinter import filedialog as tkd
    iterIzip = zip
    xrange = range
import csv
import itertools
import time
import cv2
from scipy import stats
from datetime import datetime


def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def present_time():
        return datetime.now().strftime('%Y%m%d_%H%M%S')

def getFolder(initialDir):
    '''
    GUI funciton for browsing and selecting the folder
    '''    
    root = tk.Tk()
    initialDir = tkd.askdirectory(parent=root,
                initialdir = initialDir, title='Please select a directory')
    root.destroy()
    return initialDir+'/'

def getDirList(folder):
    return natural_sort([os.path.join(folder, name) for name in os.listdir(folder) if os.path.isdir(os.path.join(folder, name))])

def getFiles(dirname, extList):
    filesList = []
    for ext in extList:
        filesList.extend(glob.glob(os.path.join(dirname, ext)))
    return natural_sort(filesList)

def readCsv(csvFname):
    rows = []
    with open(csvFname, 'r') as csvfile: 
        csvreader = csv.reader(csvfile) 
        for row in csvreader: 
            rows.append(row) 
    return rows


#===========================================================
def displayIms(imStack, fps, winName):
    '''
    displays the images contained in imList according to the defined fps
    '''
    try:
        cv2.namedWindow(winName, cv2.WINDOW_NORMAL)
    except:
        pass
    for i,img in enumerate(imStack):
        cv2.imshow(winName,img)
        if cv2.waitKey(int(1000/fps)) & 0xFF == ord('q'):
          break    
    cv2.destroyAllWindows()

#===========================================================
def readMov(movPath, swapImAxes):
    '''
    returns a list of frames read from the movie
    '''
    if swapImAxes == True:
        cap = cv2.VideoCapture(movPath)
        ret, frame = cap.read()
        w,h,_ = frame.shape
        cap.release()
    cap = cv2.VideoCapture(movPath)
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")
    imgStack = []
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            if swapImAxes == True:
                frame = cv2.resize(frame, (w,h))
            imgStack.append(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
        else:
            break
    cap.release()
    return imgStack

def imRead(x, swapImAxes):
    if swapImAxes == True:
        img = cv2.cvtColor(cv2.imread(x), cv2.COLOR_BGR2GRAY)
        w,h,_ = img.shape
        return cv2.resize(img, (w,h))
    else:
        return cv2.cvtColor(cv2.imread(x), cv2.COLOR_BGR2GRAY)#, cv2.IMREAD_GRAYSCALE)

def readIms(imDir, imExt, pool, swapImgAxes):
    imList = getFiles(imDir, imExt)
    imgStack = np.array(pool.map(imRead, iterIzip(imList, swapImgAxes)), dtype=np.uint8)
    return imgStack








#===========================================================
def tracknSave_accWeight(imData, weight, avg):
    '''
    tracks object in the given frame on the basis of parameters set in params
    tracking is done by cv2.accumulateWeighted
    Input parameters: 
        imData: image array
        weight: weight for the accumulated weight for image averaging
        avg: the averaged image from previous frames
    Output parameters:
        avg:  the averaged image produced by cv2.accumulateWeighted using previous avg image
        np.std(frameDelta): standard deviation of the image array, this is used to determine movement in the image
    '''
    cv2.accumulateWeighted(imData, avg, weight)
    frameDelta = cv2.absdiff(imData, cv2.convertScaleAbs(avg))
    return avg, float(np.std(frameDelta)), frameDelta


def getBgIm(imgs):
    '''
    returns a background Image for subtraction from all the images using weighted average (median based)
    '''
    avg = np.array((np.median(imgs, axis=0)), dtype = np.uint8)
    return cv2.convertScaleAbs(avg)


def getBgSubIms(inImgstack, bgIm):
    '''
    returns the stack of images after subtracting the background image from the input imagestack
    '''
    subIms = np.zeros(np.shape(inImgstack), dtype = np.uint8)
    for f in range(0, len(inImgstack)):
        subIms[f] = cv2.absdiff(inImgstack[f], bgIm)
    return subIms
    
def getImContours(args):
    '''
    returns the list of detected contour
    input:
        im:  image numpy array to be processed for contour detection
        params: dictionary of all parameters used for contour detection
        params :
            blurkernel
            block
            cutoff
            ellratio
            ellAxisRatioMin
            ellAxisRatioMax
            flyAreaMin
            flyAreaMax
    '''
    im = args[1].copy()
    params = args[2]
    contour = []
    blur = cv2.GaussianBlur(im,(params['blurKernel'], params['blurKernel']),0)
    ret,th = cv2.threshold(blur, params['threshLow'], params['threshHigh'],cv2.THRESH_BINARY)
    tmp = cv2.findContours(th, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    contours = tmp[0] if len(tmp) == 2 else tmp[1]
    try:
        contours = sorted(contours, key = cv2.contourArea)[-10:]
        ellRatio = [(float(cv2.fitEllipse(cnt)[1][0])/cv2.fitEllipse(cnt)[1][1], cv2.contourArea(cnt), cnt) for cnt in contours ]
        for cnt in ellRatio:
            if params['ellaxisRatioMin']<cnt[0]<params['ellaxisRatioMax'] and params['flyareaMin']<cnt[1]<params['flyareaMax']:
                contour.append([args[0], 
                                cv2.fitEllipse(cnt[2]),
                                cv2.contourArea(cnt[2]),
                                args[3]])
            else:
                pass
    except:
        pass
    if contour==[]:
        contour.append([args[0],  [], args[3]])
    return contour

def getFlycontour(imgs, contourParams, header, pool, dispIms):
    '''
    tracks the fly using cv2.SimpleBlobDetector method and saves the tracked flies in folders
    '''
    params = contourParams
    nImsToProcess = len(imgs)
    startTime = time.time()
    bgIm = getBgIm(imgs)
    subIms = getBgSubIms(imgs, bgIm)
    if dispIms == True:
        displayIms(subIms, 30, 'subIMs')
    poolArgList = iterIzip(np.arange(nImsToProcess), subIms, itertools.repeat(params), np.arange(nImsToProcess))
    imgWithCnt = pool.map(getImContours, poolArgList)
    t = time.time()-startTime
    print("imRead and Contours detection time for %d frames: %0.3f Seconds at %0.3f FPS\n"%(len(imgWithCnt),t ,len(imgWithCnt)/float(t)))
    contoursList = [header]
    for idx, i in enumerate(imgWithCnt):
        fname = str(idx)
        if i[0][1] != []:
            coordX = i[0][1][0][0]
            coordY = i[0][1][0][1]
            minorAxis = i[0][1][1][0]
            majorAxis = i[0][1][1][1]
            angle = i[0][1][2]
            area = i[0][2]
            contoursList.append([fname, coordX, coordY, minorAxis, majorAxis, angle, area,])
        else:
            contoursList.append([fname,0,0,0,0,0,0])
    return contoursList




#===========================================================
trackDataCsvHeader = ['trackDetails',
                      'distance travelled (px)',\
                      'track duration (frames)',\
                      'average instantaneuos speed (px/s)',\
                      'median body length, BLU (px)',\
                      'path straightness (r^2)',\
                      'geotactic Index',\
                      'FPS',\
                      'Pixel Size (um/px)',\
                      'skipped frames threshold (#frames)',\
                      'track duration threshold (#frames)',
                    ]


#===========================================================
def getTrackDirection(trackData, minDis):
    '''
    returns a +1 or -1 based on direction of fly movement.
    If the fly walks from left to right  it returns -1 (equivalent to bottom to top for climbing)
    if the fly walks from right to left, it returns +1 (equivalent to top to bottom for climbing)
    Value is calculated purely on the basis of a line fit on the track based on change of X-coordinate w.r.t frames
    '''
    dataLen = len(trackData)
    m,c,r,_,_ = stats.linregress(np.arange(dataLen), trackData[:,0])
    delta = (m*(9*(dataLen/10))+c)-(m*(dataLen/10)+c)
    if delta>=minDis:
        return -1, r
    elif delta<=-minDis:
        return 1, r
    else:
        return 0, r

def getEuDisCenter(pt1, pt2):
    return np.sqrt(np.square(pt1[0]-pt2[0])+np.square(pt1[1]-pt2[1]))

def getTotEuDis(xyArr):
    xyArr = np.array(xyArr)
    n = xyArr.shape[0]
    totDis = np.zeros((n-1))
    for i in xrange(0, n-1):
        totDis[i] = getEuDisCenter(xyArr[i], xyArr[i+1])
    return totDis

def intermediates(p1, p2, nb_points=8):
    """"Return a list of nb_points equally spaced points between p1 and p2
    https://stackoverflow.com/questions/43594646/how-to-calculate-the-coordinates-of-the-line-between-two-points-in-python
    """
    # If we have 8 intermediate points, we have 8+1=9 spaces
    # between p1 and p2
    x_spacing = (p2[0] - p1[0]) / (nb_points + 1)
    y_spacing = (p2[1] - p1[1]) / (nb_points + 1)

    return np.array([[p1[0] + i * x_spacing, p1[1] +  i * y_spacing] 
            for i in range(1, nb_points+1)])

def diff(x):
    return x[1]-x[0]

def getTrackBreaksPts(xyArray):
    '''
    returns the breakpoints in the XYarray of the centroids of the fly (determined by the '0' in the array)
    '''
    trackStop = 0
    nTracks = []
    for f,x in enumerate(xyArray):
        if x[0]==0 and x[1]==0:
            if trackStop == 0:
                trackStop = f
                nTracks.append([trackStop])
        else:
            if trackStop != 0:
                nTracks[-1].extend([f])
                trackStop = 0
    if trackStop != 0:
        nTracks[-1].extend([f])
    return nTracks

def extrapolateTrack(xyArray, breakPoints, skippedFrThresh, verbose=True):
    '''
    fills the gaps in the xyArray determined by breakPoints, if the gap is less
        than skippedFrThresh
    
    '''
    splitTracks = []
    trackStart = 0
    arrCopy = xyArray.copy()
    for i,x in enumerate(breakPoints):
        if len(x)>1 and diff(x)>0:
            trackBrkLen = diff(x)
            if verbose:
                print(trackBrkLen, x)
            if trackBrkLen >= skippedFrThresh:
                splitTracks.append([trackStart, x[0]])
                trackStart = x[1]
            else:
                arrCopy[x[0]:x[1],:] = intermediates(xyArray[x[0]-1,:], xyArray[x[1],:], trackBrkLen)
    if (x[0] - trackStart)>=skippedFrThresh:
        splitTracks.append([trackStart, x[0]])
    return splitTracks, arrCopy



#===========================================================
def calcParams(flyContourData, tdParams):
        trackNumber = 0
        trackDataOutput = []
        fps =               tdParams['FPS']
        mvmntStopThresh =   tdParams['noMvmtThresh']
        statsFileHeader =   tdParams['statsFileHeader']
        trackDurThresh =    tdParams['trackDurThresh'] 
        threshTrackLen =    tdParams['trackLenThresh'] 
        pxSize =            tdParams['pixelSixe'] 
        VideoDataName =     tdParams['VideoDataName']
        headerRowId =       tdParams['headerRowId']
        
        threshTrackDur = fps * trackDurThresh
        skpdFramesThresh = mvmntStopThresh*fps
        colIdBodyLen = statsFileHeader.index('majorAxis (px)')
        #---get contig track from one csv file----#
        centroids = np.array([x[1:3] for i,x in enumerate(flyContourData) if i>0], dtype=np.float64)
        if len(centroids)>skpdFramesThresh:
            brkPts = getTrackBreaksPts(centroids)
            if brkPts!=[]:
                contigTracks, cnts_ = extrapolateTrack(centroids, brkPts, skpdFramesThresh, verbose=False)
            else:
                print('no track to split, full length track present')
                contigTracks = [[0, len(centroids)]]
            trackLengths = [diff(x) for _,x in enumerate(contigTracks)]
            thresholdedTracksDur = [x for _,x in enumerate(trackLengths) if x>threshTrackDur]
            threshTrackFrNum = [contigTracks[trackLengths.index(x)] for _,x in enumerate(thresholdedTracksDur)]
            
            #---- get data from each contig track from a csv file ----#
            for i,trk in enumerate(threshTrackFrNum):
                trackData = flyContourData[headerRowId+1:][trk[0]:trk[1]]
                centroids = np.array([x[1:3] for i_,x in enumerate(trackData)], dtype=np.float64)
                brkPts = getTrackBreaksPts(centroids)
                if brkPts!=[]:
                    _, cnts = extrapolateTrack(centroids, brkPts, skpdFramesThresh, verbose=False)
                else:
                    cnts = centroids
                bodyLen = np.array([x[colIdBodyLen] for i_,x in enumerate(trackData) if float(x[colIdBodyLen])>0], dtype=np.float64)
                instanDis = getTotEuDis(cnts)
                gti, rSquared = getTrackDirection(cnts, threshTrackLen)
                trackDataOutput.append([VideoDataName+'_'+str(i), 
                                        np.sum(instanDis), 
                                        len(cnts),
                                        np.mean(instanDis),
                                        np.median(bodyLen),
                                        rSquared, gti, fps, pxSize,
                                        skpdFramesThresh, threshTrackDur,
                                        ])
                trackNumber+=1
            return trackDataOutput
        else:
            print('Number of frames less than movie length minimum threshold')
            return []









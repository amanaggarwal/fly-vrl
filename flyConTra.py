#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 19:47:19 2019

@author: aman

1)  Load the data (from frame folder/ movie)
2)  Split movies based on the motion detection
3)  Track a fly based on its contours
4)  Create a CSV file for each detected track
5)  Create a  compiled CSV for all tracks detected in the given image data (folder/movie)


Motion detection is based on the upper and lower thresholds for the standard deviation (SD)
    of the signal
The movie is captured if the SD of the captured signal is between the set thresholds
    If the SD is less than the lower threshold (no motion), no frame is saved
    If the SD is greater than the upper threshold (too much motion, possibly 
        roation of the arena), then also no frames are saved

Also, a threshold of minimum length of a movie can be set. 
    If the frames with motion are less than the movie length threshold, no movie is
        captured

Steps:
    1)  Select whether you want to process frames or movies
    2)  Define the approximate FPS at with the movie is captured. Default value is 30FPS
    3)  Try to swap axes if the data is not loaded properly
 
    6)  Have an option of displaying the selected frames with motion, with yellow overlay 
        on the position bar probably

"""

import sys
if  sys.version_info[0]<3:
    import Tkinter as tk
    import tkFileDialog as tkd
else:
    import tkinter as tk
    from tkinter import filedialog as tkd

import baseFunctions as bf

import multiprocessing as mp
import csv


#===========================================================
def selectImFolder():
    global imgStack, VideoDataName
    imgStack = []
    VideoDataName = tkd.askdirectory()
    imgStack = bf.readIms(VideoDataName, imExt, pool, swapImgAxes.get())
    print('Read Images')
    if dispMov.get() == True:
        bf.displayIms(imgStack, fps, 'Loaded Frames')
    trackDataParams['VideoDataName'] = VideoDataName
    print(VideoDataName)

def selectMovie():
    global imgStack, VideoDataName
    imgStack = []
    VideoDataName = tkd.askopenfilename(filetypes = (("Video files","*.avi *.mp4"),("all files","*.*")))
    imgStack = bf.readMov(VideoDataName, swapImgAxes.get())
    print('Read Movie')
    if dispMov.get() == True:
        bf.displayIms(imgStack, fps, 'Loaded Video File')
    trackDataParams['VideoDataName'] = VideoDataName
    print(VideoDataName)

#===========================================================
def processBodyParams():
    global flyTracks, fps
    fps = int(fpsSpin.get())
    trackDataParams['FPS'] = int(fpsSpin.get())
    avg  = None
    flyFirstFrame = False
    flyTracks = []
    allTrackData = []
    if imgStack != []:
        avgFrames = []
        print('Processing for Body parameters')
        print("\n[INFO] starting background model...")
        avg = imgStack[0].copy().astype("float")
        for i, imData in enumerate(imgStack):
            avg, frDelta, avgIm = bf.tracknSave_accWeight(imData, avgWeight, avg)
            avgFrames.append(avgIm)
            if framedelThreshMin < frDelta < framedelThreshMax and flyFirstFrame == False:
                flyFirstFrame = True
                flyTracks.append([])
            if framedelThreshMin < frDelta < framedelThreshMax and flyFirstFrame == True:
                flyTracks[-1].append(imData)
            if (frDelta < framedelThreshMin or frDelta > framedelThreshMax) and flyFirstFrame == True:
                flyFirstFrame = False
        if dispMov.get() == True:
            bf.displayIms(avgFrames, fps, 'Motion Detection')
        if flyTracks != []:
            for i, ft in enumerate(flyTracks):
                if dispMov.get() == True:
                    bf.displayIms(ft, fps, 'Segregated tracks')
                flyContours = bf.getFlycontour(ft, params, statsFileHeader, pool, dispMov.get())
                statsFile = VideoDataName+'_'+bf.present_time()+'_stats_'+str(i)+'.csv'
                with open(statsFile, "w") as f:
                    writer = csv.writer(f)
                    writer.writerows(flyContours)
                trackData = bf.calcParams(flyContours, trackDataParams)
                if trackData != []:
                    trackData[0][0] = statsFile
                    allTrackData.append(trackData)
        trackFile = VideoDataName+'_'+bf.present_time()+'_trackData.csv'
        with open(trackFile, "w") as f:
            csvWriter = csv.writer(f)
            csvWriter.writerow(bf.trackDataCsvHeader)
            for _, data in enumerate(allTrackData):
                csvWriter.writerows(data)
    else:
        print('no data selected to process')
    print('Processed for Motion detection')


def validate_input(new_value):
    if new_value != '':
        valid = new_value .isdigit() and len(new_value) <= 5
        return valid
    else:
        valid = True
        return valid


#===========================================================
def GUI():
    global swapImgAxes, dispMov, fpsSpin
    root = tk.Tk()
    
    btnImg = tk.Button(root, text="Select Image folder", command=selectImFolder)
    btnMov = tk.Button(root, text="Select Movie", command=selectMovie)
    
    fpsLabel = tk.Label(root, text = 'Set Imaging FPS')
    
    validate = root.register(validate_input)

    fpsSpin = tk.Entry(root, validate="key", textvariable = tk.StringVar(root, fps), validatecommand=(validate, "%P"))
#    fpsSpin.setvar(str(fps))
    swapImgAxes = tk.BooleanVar()
    swapImgAxes.set(False)
    imAxSwap = tk.Checkbutton(root, text="Swap Image Axes", variable = swapImgAxes, onvalue = True, offvalue = False)
    
    dispMov = tk.BooleanVar()
    dispMov.set(False)
    disp = tk.Checkbutton(root, text="Display video data", variable = dispMov, onvalue = True, offvalue = False)
    
    btnBodyTrk = tk.Button(root, text="Process for Body tracking", command=processBodyParams)
    
    btnImg.pack()#side="left", fill="both", expand="yes", padx="10", pady="10")
    btnMov.pack()#side="right", fill="both", expand="yes", padx="10", pady="10")
    fpsLabel.pack()
    fpsSpin.pack()
    imAxSwap.pack()
    disp.pack()
    btnBodyTrk.pack()#side="bottom", fill="both", expand="yes", padx="10", pady="10")
    
    root.mainloop()







fps = 30

imExt = ['*.jpeg', '*.jpg', '*.JPEG', '*.JPG', '*.png', '*.PNG']
#--- initializing the Variables ---#
swapImgAxes = False
dispMov = True

headerRowId = 0             # index of the header in the CSV file
flyFirstFrame = False       #variable to keep track when the fly entered the imaging arena
imgStack = []
VideoDataName = ''
flyTracks = []
fpsSpin = ''


#--- Hyperparameters for detection of fly based on contour ---#
params = {}                 # dict for holding parameter values for contour detection

params['threshLow'] = 45    #lower bound for pixel values
params['threshHigh'] = 255  #upper bound for pixel values

params['ellaxisRatioMin'] = 0.2
params['ellaxisRatioMax'] = 0.7

params['flyareaMin'] = 200  #lower bound for fly area (for 0.0546875 mm/px pixel size)
params['flyareaMax'] = 2100 #upper bound for fly area (for 0.0546875 mm/px pixel size)

params['blurKernel'] = 11   #kernel size for Gaussian blur for smoothening the noise

statsFileHeader = ['frameDetails',
                   'x-coord',
                   'y-coord',
                   'minorAxis (px)', 
                   'majorAxis (px)', 
                   'angle', 
                   'area (px)']


#--- Hyperparameters for processing of fly track from detected fly contours ---#
trackDataParams = {}    # dict for holding parameter values for flytrack data processing
trackDataParams['FPS'] = fps
trackDataParams['noMvmtThresh'] = 0.05      # percentage of frames that can be skipped for fly detection
                                            #       for a track to be counted as a contigous track
trackDataParams['trackDurThresh'] = 0.2     # threshold of track duration, in seconds
trackDataParams['trackLenThresh'] = 1       # in BLU
trackDataParams['pixelSixe'] = 70.00/1280   # in mm/pixel
trackDataParams['statsFileHeader'] = statsFileHeader
trackDataParams['VideoDataName'] = VideoDataName  
trackDataParams['headerRowId'] = headerRowId


#---Motion detection hyperparameters---#
framedelThreshMin = 1       #Lower threshold for SD based motion detection
framedelThreshMax = 15      #Upper threshold for SD based motion detection
avgWeight = 0.01#0.001      #factor for averaging frames for motion detection
#---- declared all the hyperparameters above ----#


#avg = None                  #starting avg image

nThreads = 8
pool = mp.Pool(processes=nThreads)
GUI()
pool.close()



























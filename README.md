# fly-VRL
This repo contains the data for the article "A locomotor assay reveals deficits in heterozygous Parkinson's disease model and proprioceptive mutants in adult Drosophila"

DOI: 	https://doi.org/10.1073/pnas.1807456116

PubMed:	https://www.ncbi.nlm.nih.gov/pubmed/31748267

--> The zip file named 'csvData.zip' contains the raw digitized data extracted from the video files

--> The file named "flyConTra.py" is the script for motion detection, tracking and digitizing fly in the behaviour arena


The input can be: 
	A folder containing individual frames captured
		OR
	A video file

The script uses hyperparamteres defined in the file XXXX to 
	a) Automatically detect motion of the fly in a non-moving arena
	b) Track and capture data of fly movement for each detected track
	c) Compile data for each folder/movie detected
	d) save all the data as CSV files in the directory containing the image folder/ movie



This code is implemented in Python and works with Python2.7 and Python3.4.3
imported libraries:

numpy, 
os, 
glob, 
re, 
Tkinter, 
tkFileDialog, 
csv, 
itertools, 
time, 
cv2, 
scipy (Version >= 1.1.0), 
datetime, 
multiprocessing, 

The script can be executed by typing:

$python flyConTra.py


